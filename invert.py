"""
OVERVIEW
CS4300 Programming Assignment 1
Wit Tatiyanupanwong (NetID wt255)

CORPUS
Test data will be read from "test/file##.txt" where ## = 00 to 39
the program also uses ## number as doc-id

IMPORTANT DATA STRUCTURE (more detail in the report)
wordList    - a dict mapping term to the list [posting list]
              that is { term(string) : [ postingList(see below)  ] }
postingList - (wordList[term]) is a dict mapping doc-id in which the term occur
              to the list of position of the occurence of term in docID
              that is { docID : [ pos1, pos2, pos3 ... posN ] }
locationList- (wordList[term][docID]) is a list of locations in which
              the "term" occurs in the given document "DocID"
                         
              Note that document occurance and term occurance are not explicitly stored
              because it is essentially len(wordList) and len(postingList). 
              The runtime complexity of len() on dict is O(1) according to 
              http://wiki.python.org/moin/TimeComplexity

ACKNOWLEDGEMENT
The program includes Martin Porter's Porter Stemming algorithm
implemented in Python by Vivake Gupta (v@nano.com)
The source code is freely available at from http://tartarus.org/~martin/PorterStemmer/
"""

from porter_stemmer import PorterStemmer
import math

# update wordlist and postinglist from a given document id/filepath
def analyzeDocument(wordList,stopList,docID,docFile):
    try:
        docText = docFile.read()
        
        tokens = [token.strip().lower() for token in docText.split(' ')]
        tokens = porterStemmer(tokens)
                
        # for each token
        for position, token in enumerate(tokens):
            # skip words in stoplist
            if token in stopList:
                continue
            
            # first occurence of word
            if token not in wordList:
                wordList[token] = {}     # initialize Word List for the new token
                
            # first occurence of docID
            if docID not in wordList[token]:   # initialize Posting List for the new document
                wordList[token][docID] = []    # - create an empty position list
                
            # save position found
            wordList[token][docID].append(position)
    finally:
        docFile.close()
    return wordList


# use index to process user query
def testQuery(wordList,stopList,testDocFiles,query):
    # tokenize with single space
    tokens = [token.strip().lower() for token in query.split(' ')] 
    # filter using stoplist
    tokens = [token for token in tokens if token not in stopList]
    # stem
    tokens = porterStemmer(tokens)
    
    # if the query string has one token
    if len(tokens) == 1:
        term = tokens[0]
        if term not in wordList:
            print term + " does not match any document.\n"
            return
        for docID in wordList[term]:
            # prepare tf idf tf*idf info and document exerpt near the first match
            stat = calculateStatistic(wordList,term,docID,len(testDocFiles))
            hitPos = wordList[term][docID][0]
            docFile = open(testDocFiles[docID])
            hitPos = 0 if (hitPos-5)<0 else hitPos-5 # adjsut context words to print
            docExcerpts = docFile.read().split(' ')[hitPos:hitPos+11]
            docFile.close()
 
            # print one search result
            print "%0.2f, %0.2f, %0.2f" % stat
            print "%02d : [%s]" % (docID, ','.join(map(str,wordList[term][docID])))
            print "%s\n" % (' '.join(docExcerpts))
            
    # if query has multiple tokens
    elif len(tokens) > 1:
        # make sure every tokens has at least a hit
        matchedTokens = [token for token in tokens if token in wordList]
        if matchedTokens != tokens:
            print "Sorry, no document have all the specified query terms.\n" 
            return
        
        # a set of all posting list of matched documents
        matchedPostingList = [ wordList[token].keys() for token in matchedTokens ]
        matchedPostingList = sorted(matchedPostingList, key=len) # sort by length from shortest
        
        # intersection of every posting lists (start from the shortest list)
        matchedDocs = None
        for postingList in matchedPostingList:
            matchedDocs = set(postingList) if matchedDocs is None \
                          else matchedDocs.intersection(set(postingList))
        
        # calculate tf*idf for each document
        tf_idf = {}
        for docID in matchedDocs:
            tf_idf[docID] = sum([ calculateStatistic(wordList,term,docID,len(testDocFiles))[2] \
                                  for term in matchedTokens ])
            
        # sort search result by tf*idf and show to the user
        rankedDocs = sorted(tf_idf, key=tf_idf.get, reverse=True)
        for docID in rankedDocs:
            print "%02d  %0.2f" % (docID, tf_idf[docID])
        print ""
    # if all query string filtered by stoplist
    else:
        print "Sorry, all words in the query string are filtered by stop list\n"
        
# return (tf, idf, tf.idf)
def calculateStatistic(wordList,term,docID,N):
    # calcualte term frequency
    f_td = len(wordList[term][docID])
    tf   = math.log(f_td, 10)+1 if f_td > 0 else 0
    
    # calculate inverse document frequency
    Nt   = len(wordList[term])
    idf  = math.log(N/float(Nt), 10)
    
    return (tf, idf, tf*idf)

def porterStemmer(tokens):
    p = PorterStemmer()
    tokens = [p.stem(token, 0,len(token)-1) for token in tokens]
    return tokens

def main():
    # 1. Inverted file setup
    # ----------------------
    testDocFiles = {}
    for docID, docPath in [(id,"test/file%02d.txt" % (id)) for id in range(40)]:
        testDocFiles[docID] = docPath

    stopFile = open("stoplist.txt")
    stopList = stopFile.read().split()
    stopFile.close()
    
    # analyze each document one by one
    wordList = {}
    for docID in testDocFiles:
        analyzeDocument(wordList, stopList, docID, open(testDocFiles[docID]))
    
    # produce word listing as shown in the report
    """
    print "DATA DUMP [190:195]\n"
    firstFewTerms = sorted(wordList.keys())[190:195]
    print "Word list " + str(firstFewTerms) + "\n"
    print "Posting list"
    for term in firstFewTerms:
        print "  wordList[%s] %s" % (term,wordList[term])
    print ""
    """
    
    # 2. Test inteface
    # ----------------
    while True:
        query = raw_input("Enter query: ")
        if query == "ZZZ":
            break
        elif query.strip() == "":
            continue
        else:
            print("")
            testQuery(wordList,stopList,testDocFiles,query)
    print "\nProgram terminated."
      

if __name__ == '__main__':
    main()
    
